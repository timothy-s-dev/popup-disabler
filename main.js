var disablerFunction = function () {
  console.log("Overriding alert() and confirm()...");
  window.alert = function alert(msg) { console.log('Alert: ' + msg); };
  window.confirm = function confirm(msg) {
    console.log("Confirm (auto-accepted): " + msg);
    return true;
  };
};
var disablerCode = "(" + disablerFunction.toString() + ")();";

var disablerScriptElement = document.createElement('script');
disablerScriptElement.textContent = disablerCode;

document.documentElement.appendChild(disablerScriptElement);
disablerScriptElement.parentNode.removeChild(disablerScriptElement);