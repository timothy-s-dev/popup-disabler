# Popup Disabler

This is a small Vivaldi/Chrome extension to disable popups for given URLs.
It does this by overriding `window.alert()` and `window.confirm()` to prevent popups from opening.

## Installation

The extension is not currently available on the chrome web store, as it may require some tweaking of the source code to get the behavior you want.  To install the extension, clone or download this repository, then load the unpacked extension manually by navigating to `vivaldi://extensions` or `chrome://extensions`, and clicking the "Load unpacked" button.  You may need to toggle developer mode on first, using the toggle switch on that same page.

## Setup - READ BEFORE USE

By default, the extension only affects pages at one URL: `https://www.improved-initiative.com/*` &mdash; to change this to disable popups on other pages you need to modify the value of `content_scripts.matches` in the manifest.json file for this extension.  Instructions for how to format the values there can be found in the [Chrome Developer Documentation](https://developer.chrome.com/extensions/match_patterns).

**WARNING:** The extension automatically *accepts* `window.confirm()` popups by default.  To change this, open the `main.js` source file, and change line 6 to `return false;`

**Note:** Unpacked extensions are not updated automatically - if you make changes to the manifest or the source code you will need to reload the extension for the changes to take effect.
